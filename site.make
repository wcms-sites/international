core = 7.x
api = 2

; nodeblock
projects[nodeblock][type] = "module"
projects[nodeblock][download][type] = "git"
projects[nodeblock][download][url] = "https://git.uwaterloo.ca/drupal-org/nodeblock.git"
projects[nodeblock][download][tag] = "7.x-1.6"
projects[nodeblock][subdir] = ""

; uw_waterloo_international
projects[uw_waterloo_international][type] = "module"
projects[uw_waterloo_international][download][type] = "git"
projects[uw_waterloo_international][download][url] = "https://git.uwaterloo.ca/wcms/uw_waterloo_international.git"
projects[uw_waterloo_international][download][tag] = "7.x-1.0"
projects[uw_waterloo_international][subdir] = ""

; uw_ct_exchange_program
projects[uw_ct_exchange_program][type] = "module"
projects[uw_ct_exchange_program][download][type] = "git"
projects[uw_ct_exchange_program][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_exchange_program.git"
projects[uw_ct_exchange_program][download][tag] = "7.x-1.5"
projects[uw_ct_exchange_program][subdir] = ""

; uw_ct_exchange_program_overridden
projects[uw_ct_exchange_program_overridden][type] = "module"
projects[uw_ct_exchange_program_overridden][download][type] = "git"
projects[uw_ct_exchange_program_overridden][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_exchange_program_overridden.git"
projects[uw_ct_exchange_program_overridden][download][tag] = "7.x-1.2"
projects[uw_ct_exchange_program_overridden][subdir] = ""

